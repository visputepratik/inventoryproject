﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using InventoryProject.Models;

namespace InventoryProject.Controllers
{
    public class ProductAPIController : ApiController
    {
        InventoryEntities inventoryEntities = new InventoryEntities();
        [HttpGet]
        public IHttpActionResult ListProduct()
        {
            
            var results = inventoryEntities.products.ToList();
            return Ok(results);
        }

        [HttpPost]
        public IHttpActionResult AddProduct(product insertProduct)
        {
            inventoryEntities.products.Add(insertProduct);
            inventoryEntities.SaveChanges();
            return Ok();
        }

        [HttpGet]
        public IHttpActionResult ProductDetails(int id)
        {
            Product productDetails = null;
            productDetails = inventoryEntities.products.Where(x => x.productId == id).Select(x => new Product()
            {
                productId = x.productId,
                name = x.name,
                description = x.description,
                price = x.price,
                quantity = x.quantity
            }).FirstOrDefault<Product>();
            if(productDetails == null)
            {
                return NotFound();
            }

            return Ok(productDetails);
        }

        [HttpPut]
        public IHttpActionResult UpdateProduct(Product product)
        {
            var udpateProduct = inventoryEntities.products.Where(x => x.productId == product.productId).FirstOrDefault<product>();
            if (udpateProduct != null)
            {
                udpateProduct.productId = product.productId;
                udpateProduct.name = product.name;
                udpateProduct.description = product.description;
                udpateProduct.price = product.price;
                udpateProduct.quantity = product.quantity;
                inventoryEntities.SaveChanges();
            }
            else
            {
                return NotFound();
            }

            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var deleteProduct = inventoryEntities.products.Where(x => x.productId == id).FirstOrDefault();
            inventoryEntities.Entry(deleteProduct).State = System.Data.Entity.EntityState.Deleted;
            inventoryEntities.SaveChanges();
            return Ok();
        }
    }
}
