﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using InventoryProject.Models;
using System.Net.Http;

namespace InventoryProject.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            IEnumerable<product> productObj = null;
            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44340/api/ProductAPI");

            var consumeAPI = hc.GetAsync("ProductAPI");
            consumeAPI.Wait();

            var readData = consumeAPI.Result;
            if (readData.IsSuccessStatusCode)
            {
                var displayData = readData.Content.ReadAsAsync<IList<product>>();
                displayData.Wait();

                productObj = displayData.Result;
            }
            return View(productObj);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(product insertProduct)
        {
            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44340/api/ProductAPI");

            var insertRecord = hc.PostAsJsonAsync<product>("ProductAPI", insertProduct);
            insertRecord.Wait();

            var saveData = insertRecord.Result;
            if (saveData.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return View("Create");
        }

        public ActionResult Details(int id)
        {
            Product productObj = null;

            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44340/api/");

            var consumeAPI = hc.GetAsync("ProductAPI?id=" + id.ToString());
            consumeAPI.Wait();

            var readData = consumeAPI.Result;
            if (readData.IsSuccessStatusCode)
            {
                var displayData = readData.Content.ReadAsAsync<Product>();
                displayData.Wait();
                productObj = displayData.Result;
            }
            return View(productObj);
        }

        public ActionResult Edit(int id)
        {
            Product productObj = null;

            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44340/api/");

            var consumeAPI = hc.GetAsync("ProductAPI?id=" + id.ToString());
            consumeAPI.Wait();

            var readData = consumeAPI.Result;
            if (readData.IsSuccessStatusCode)
            {
                var displayData = readData.Content.ReadAsAsync<Product>();
                displayData.Wait();
                productObj = displayData.Result;
            }
            return View(productObj);
        }

        [HttpPost]
        public ActionResult Edit(Product product)
        {
            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44340/api/ProductAPI");

            var insertRecord = hc.PutAsJsonAsync<Product>("ProductAPI", product);
            insertRecord.Wait();

            var saveData = insertRecord.Result;
            if (saveData.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.message = "Product not Updated ......";
            }
            return View("Create");
        }

        public ActionResult Delete(int id)
        {
            HttpClient hc = new HttpClient();
            hc.BaseAddress = new Uri("https://localhost:44340/api/ProductAPI");

            var deleteProduct = hc.DeleteAsync("ProductAPI/" + id.ToString());
            deleteProduct.Wait();

            var displayData = deleteProduct.Result;
            if (displayData.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return View("Index");
        }
    }
}